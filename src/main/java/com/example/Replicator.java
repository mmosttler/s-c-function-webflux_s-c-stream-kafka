package com.example;

import java.util.function.Consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import com.example.beans.SampleRequest;

import reactor.core.publisher.Flux;

/**
 * 
 */
@SpringBootApplication
@EnableBinding(Source.class)
public class Replicator
{
    public static void main(String[] args)
    {
        SpringApplication.run(Replicator.class, args);
    }

    @Bean
    public Consumer<Flux<SampleRequest>> replicate(ApplicationContext context) 
    {
        return payloads -> payloads.log().map(objectToSave -> {
            MessageChannel channel = context.getBean(Source.OUTPUT, MessageChannel.class);
            return channel.send(MessageBuilder.withPayload(objectToSave).build());
        }).subscribe();
    }
}
