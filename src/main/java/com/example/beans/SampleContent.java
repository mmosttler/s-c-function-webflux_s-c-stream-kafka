package com.example.beans;

/**
 * 
 */
public class SampleContent
{
    private String token;
    private String value;
    
    public final String getToken()
    {
        return token;
    }
    public final void setToken(String token)
    {
        this.token = token;
    }
    public final String getValue()
    {
        return value;
    }
    public final void setValue(String value)
    {
        this.value = value;
    }

}
