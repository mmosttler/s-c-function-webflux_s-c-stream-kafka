# s-c-function-webflux_s-c-stream-kafka

Example project using spring-cloud-function and spring-cloud-stream with Kafka.  
The function takes http input and outputs to a kafka topic ( http -> function consumer -> kafka )

```
curl -X POST \
  http://localhost:8080/replicate \
  -d '{
    "key": "foo9",
    "content": {
        "token": "test124",
        "value": "ww45"
    }
}'
```